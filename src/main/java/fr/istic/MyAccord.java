package fr.istic;

import javafx.scene.control.Accordion;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyAccord extends Accordion {

	@Wire
	private Button addBtn;

	@Wire
	private Textbox taskEditableText;

	@Wire
	private Listbox taskList;

	@Wire
	private Tabbox tb;

	@Wire
	private Tabpanels tbpanels;

	@Wire
	private Tabs tbtabs;


	public void addTab(String name, String techno){
		Tab newTab = new Tab();
		newTab.setLabel(name);

		Tabpanel newPanel = new Tabpanel();
		Label newLabel = new Label();
		newLabel.setValue(techno);

		newPanel.appendChild(newLabel);
		tbpanels.appendChild(newPanel);

		tbtabs.appendChild(newTab);

		tb.setSelectedIndex(2);
	}
	@Listen("onClick = #addBtn")
	public void addTechno() throws IOException {
		JSONParser parser = new JSONParser();
		JSONArray a = (JSONArray) parser.parse(new FileReader(getClass().getResource("/techno.json").toExternalForm()));

		for (Object o : a)
		{
			JSONObject techno = (JSONObject) o;
            String body;
            String name = (String) techno.get("name");
			System.out.println(name);
			String description = (String) techno.get("Description");
            body="<b>Description :\n</b>"+description+"\n";
			String etat = (String) techno.get("Etat");
            body+="<b>Etat :\n</b>"+etat+"\n";
            String plateforme = (String) techno.get("Plateforme(s)");
            body+="<b>Plateforme(s) :\n</b>"+plateforme+"\n";
            String funct = (String) techno.get("Fonctionnement du controleur");
            body+="<b>Fonctionnement du controleur :\n</b>"+funct+"\n";
            String mapp = (String) techno.get("Mapping de données");
            body+="<b>Mapping de données :\n</b>"+mapp+"\n";
            String example = (String) techno.get("Exemple");
            body+="<b>Exemple :\n</b>"+example+"\n";
            System.out.println(body);
            addTab(name, body);
		}
			//String text = taskEditableText.getValue();



			/*if ( text != null && !text.isEmpty() ) {
				taskList.setItemRenderer(new ListitemRenderer<Techno>() {
					public void render(final Listitem listitem, Techno techno, final int i) throws Exception {

					}
				});

				Techno t = new Techno();
				t.setName(text);
				technos.add(t);
				ListModel lm = new ListModelList(technos, true);
				taskList.setModel(lm);
				Messagebox.show("The task name is empty", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);

		}*/
	}

	public Button getAddBtn() {
		return addBtn;
	}

	public void setAddBtn(Button addBtn) {
		this.addBtn = addBtn;
	}

	public Textbox getTaskEditableText() {
		return taskEditableText;
	}

	public void setTaskEditableText(Textbox taskEditableText) {
		this.taskEditableText = taskEditableText;
	}
}
