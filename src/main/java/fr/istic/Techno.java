package fr.istic;

import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by goas on 12/01/16.
 */
public class Techno {

    private String name;

    private  Combobox combobox = new Combobox();

    public Techno() {
        List l = new ArrayList();
        l.add("ta soeur");
        combobox.setModel(new ListModelList<Object>(l));
        combobox.setButtonVisible(true);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Combobox getCombobox() {
        return combobox;
    }

    public void setCombobox(Combobox combobox) {
        this.combobox = combobox;
    }
}
