package fr.istic;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MyViewModel extends SelectorComposer<Component> {

	@Wire
	private Button addBtn;

	@Wire
	private Textbox taskEditableText;

	@Wire
	private Listbox taskList;

	@Wire
	private Tabbox tb;

	@Wire
	private Tabpanels tbpanels;

	@Wire
	private Tabs tbtabs;


    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        addTechno();
    }

    public void addTab(String name, String techno, int nb){
		Tab newTab = new Tab();
        System.out.println("Name of tab created : " + name);
        newTab.setLabel(name);

		Tabpanel newPanel = new Tabpanel();
        Html body = new Html();

        body.setContent(techno);
		newPanel.appendChild(body);
        tbpanels.appendChild(newPanel);

		tbtabs.appendChild(newTab);

		//tb.setSelectedIndex(nb);
	}

    @AfterCompose
	public void addTechno() {
        try{

		JSONParser parser = new JSONParser();
        String path =  getClass().getResource("../../techno.json").toURI().toString().substring(5);
        JSONArray a = (JSONArray) parser.parse(new InputStreamReader(new FileInputStream(path),"UTF8"));
        int nb=0;
		for (Object o : a)
		{
			JSONObject techno = (JSONObject) o;
			String body;
			String name = (String) techno.get("name");
			System.out.println(name);
			String description = (String) techno.get("Description");
			body="<b>Description :\n</b><br>"+description+"\n";
			String etat = (String) techno.get("Etat");
			body+="<br><br><b>Etat :\n</b><br>"+etat+"\n";
			String plateforme = (String) techno.get("Plateforme(s)");
			body+="<br><br><b>Plateforme(s) :\n</b><br>"+plateforme+"\n";
			String funct = (String) techno.get("Fonctionnement du controleur");
			body+="<br><br><b>Fonctionnement du controleur :\n</b><br>"+funct+"\n";
			String mapp = (String) techno.get("Mapping de données");
			body+="<br><br><b>Mapping de données :\n</b><br>"+mapp+"\n";
			String example = (String) techno.get("Exemple");
			body+="<br><br><b>Exemple :\n</b><br>"+example+"\n";
			System.out.println(body);
			addTab(name, body, nb);
            nb++;
		}
    } catch (Exception e) {
        e.printStackTrace();
    }

	}
}
